# ics-ans-role-crss

Ansible role to deploy the Control Room Shift Setup application.

## Role Variables

```yaml
crss_image: registry.esss.lu.se/ics-software/crss
crss_tag: latest
crss_ldap_user: username
crss_ldap_passwd: secret
crss_epics_ca_addr_list: localhost
crss_frontend_rule: "Host:{{ ansible_fqdn }}"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-crss
```

## License

BSD 2-clause
