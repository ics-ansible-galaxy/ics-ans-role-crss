import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_crss_index(host):
    # This tests that traefik forwards traffic to the crss web server
    cmd = host.command("curl -H Host:ics-ans-role-crss-default -k -L https://localhost")
    assert "<title>CRSetup</title>" in cmd.stdout
